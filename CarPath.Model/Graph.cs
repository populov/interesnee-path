﻿using System;
using System.Collections.Generic;

namespace CarPath.Model
{
    public class Graph : IGraph
    {
        private readonly IDictionary<int, IDictionary<int,int>> nodes = new Dictionary<int,IDictionary<int,int>>(); 

        public void AddNode(int id) {
            if (nodes.ContainsKey(id))
                throw new ArgumentException(string.Format("Node {0} already present", id));
            nodes.Add(id, new Dictionary<int, int>());
        }

        public void AddNodes(IEnumerable<int> addNodes)
        {
            foreach (var node in addNodes)
                AddNode(node);
        }

        public void AddEdge(int from, int to, int weight)
        {
            EnsureEdge(from, to, weight);
            EnsureEdge(to, from, weight);
        }

        private void EnsureEdge(int from, int to, int weight)
        {
            if (from == to)
                return;
            if (!nodes.ContainsKey(to))
                throw new ArgumentException(string.Format("Edge to not existing node: {0}", to));
            var edges = GetEdges(from);
            if (edges == null)
                throw new ArgumentException(string.Format("Edge from not existing node: {0}", from));

            if (!edges.ContainsKey(to))
                edges.Add(to, weight);
            else if (edges[to] != weight)
                throw new ArgumentException(string.Format("Weight from {0} to {1} is already defined and equals to {2} != {3}", from, to, edges[to], weight));
        }

        public IDictionary<int, int> GetEdges(int nodeId)
        {
            IDictionary<int,int> edges;
            nodes.TryGetValue(nodeId, out edges);
            return edges;
        }

        public IEnumerable<int> GetNodes()
        {
            return nodes.Keys;
        }
    }
}
