﻿using System;

namespace CarPath.Model
{
    /// <summary>
    /// Cartesian tree = tree + heap. Insert, delete, find operations have complexity O(log N).
    /// </summary>
    public class Treap<TKey,TItem>
    {
        private static Random random = new Random();
        private static KeyExtractor getKey;
        private static KeyLess getLessKey;
        private static Comparison<TKey> keyCompare;

        private readonly TItem item;
        private readonly int rnd; // Pseudo-priority

        private readonly Treap<TKey,TItem> left;
        private readonly Treap<TKey,TItem> right;

        public delegate TKey KeyExtractor(TItem obj);
        public delegate TKey KeyLess(TItem obj);

        public Treap(TItem item, KeyExtractor keyExtractor, KeyLess keyLessGet, Comparison<TKey> keyComparison) : this(item, random.Next())
        {
            getKey = keyExtractor;
            getLessKey = keyLessGet;
            keyCompare = keyComparison;
        }

        private Treap(TItem item, int rnd, Treap<TKey, TItem> left = null, Treap<TKey, TItem> right = null)
        {
            this.item = item;
            this.rnd = rnd;
            this.left = left;
            this.right = right;
        }

        private static Treap<TKey, TItem> Merge(Treap<TKey, TItem> l, Treap<TKey, TItem> r)
        {
            if (l == null) return r;
            if (r == null) return l;

            if (l.rnd > r.rnd)
            {
                var newR = Merge(l.right, r);
                return new Treap<TKey, TItem>(l.item, l.rnd, l.left, newR);
            }
            var newL = Merge(l, r.left);
            return new Treap<TKey, TItem>(r.item, r.rnd, newL, r.right);
        }

        private void Split(TKey key, out Treap<TKey, TItem> l, out Treap<TKey, TItem> r)
        {
            Treap<TKey, TItem> newTree = null;
            if (keyCompare(getKey(item), key) <= 0)
            {
                if (right == null)
                    r = null;
                else
                    right.Split(key, out newTree, out r);
                l = new Treap<TKey, TItem>(item, rnd, left, newTree);
            }
            else
            {
                if (left == null)
                    l = null;
                else
                    left.Split(key, out l, out newTree);
                r = new Treap<TKey, TItem>(item, rnd, newTree, right);
            }
        }

        public Treap<TKey, TItem> Insert(TItem newItem)
        {
            Treap<TKey, TItem> l, r;
            Split(getKey(newItem), out l, out r);
            var n = new Treap<TKey, TItem>(newItem, random.Next());
            return Merge(Merge(l, n), r);
        }

        public Treap<TKey, TItem> Remove(TItem removeItem)
        {
            Treap<TKey, TItem> l, m, r;
            Split(getLessKey(removeItem), out l, out r);
            r.Split(getKey(removeItem), out m, out r);
            return Merge(l, r);
        }

        public TItem Minimum()
        {
            var current = this;
            while (current.left != null)
                current = current.left;
            return current.item;
        }

        public override string ToString()
        {
            return string.Format("this=[{0}], left=[{1}], right=[{2}]", item, left != null ? left.item.ToString() : "null", right != null ? right.item.ToString() : "null");
        }
    }
}
