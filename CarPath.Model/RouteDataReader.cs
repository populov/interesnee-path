﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

namespace CarPath.Model
{
    public class RouteDataReader : IRouteDataReader
    {
        private const int undefined = -1;

        public RoutingTask Parse(XmlDocument xDoc)
        {
            var nodes = new List<int>();
            var crashNodes = new List<int>(); 
            var links = new Dictionary<Tuple<int, int>, int>();

            int start = undefined, end = undefined;
            foreach (XmlElement nodeElement in xDoc.GetElementsByTagName("node"))
                ParseNode(nodeElement, nodes, links, crashNodes, ref start, ref end);
            RemoveLinksToCrashNodes(links, crashNodes);
            Verify(start, end, crashNodes);
            return new RoutingTask(BuildGraph(nodes, links), start, end);
        }

        public RoutingTask Read(string xmlFilePath)
        {
            if (!File.Exists(xmlFilePath))
                throw new FileNotFoundException("Input file, containing road net, not found - " + xmlFilePath, xmlFilePath);
            var xDoc = new XmlDocument();
            xDoc.Load(xmlFilePath);
            return Parse(xDoc);
        }

        private void RemoveLinksToCrashNodes(IDictionary<Tuple<int, int>, int> links, ICollection<int> crashNodes)
        {
            var linksToCrashNodes = links.Keys.Where(k => crashNodes.Contains(k.Item1) || crashNodes.Contains(k.Item2)).ToArray();
            foreach (var key in linksToCrashNodes)
                links.Remove(key);
        }

        private void ParseNode(XmlElement nodeElement, ICollection<int> nodes, IDictionary<Tuple<int, int>, int> links, ICollection<int> crashNodes, ref int start, ref int end)
        {
            var id = ReadId(nodeElement);
            if (IsCrashNode(nodeElement))
                crashNodes.Add(id);
            else
            {
                nodes.Add(id);
                ParseLinks(nodeElement, id, links);
            }
            if (IsRole(nodeElement, "start"))
            {
                if (start == undefined)
                    start = id;
                else
                    throw new InvalidDataException("More than one start node");
            }
            if (IsRole(nodeElement, "finish"))
            {
                if (end == undefined)
                    end = id;
                else
                    throw new InvalidDataException("More than one finish node");
            }
        }

        private static int ReadId(XmlElement nodeElement)
        {
            var idAttr = nodeElement.Attributes["id"];
            if (idAttr == null)
                throw new InvalidDataException("Node id not found");
            return int.Parse(idAttr.Value);
        }

        private static bool IsCrashNode(XmlElement nodeElement)
        {
            var status = nodeElement.Attributes["status"];
            return status != null && status.Value != null && status.Value.ToLower() == "crash";
        }


        private static bool IsRole(XmlElement nodeElement, string roleVal)
        {
            var role = nodeElement.Attributes["role"];
            return role != null && role.Value != null && role.Value.ToLower() == roleVal;
        }

        private void ParseLinks(XmlElement nodeElement, int from, IDictionary<Tuple<int,int>,int> links)
        {
            foreach (XmlElement link in nodeElement.GetElementsByTagName("link"))
            {
                var refAttr = link.Attributes["ref"];
                if (refAttr == null || string.IsNullOrEmpty(refAttr.Value))
                    throw new InvalidDataException("link element has no ref value");
                int to = int.Parse(refAttr.Value);
                var weightAttr = link.Attributes["weight"];
                if (weightAttr == null || string.IsNullOrEmpty(weightAttr.Value))
                    throw new InvalidDataException("link element has no weight value");
                int weight = int.Parse(weightAttr.Value);
                var key = new Tuple<int, int>(@from, to);
                if (links.ContainsKey(key))
                {
                    if (links[key] != weight)
                        throw new InvalidDataException(string.Format("Weight from {0} to {1} is already defined and equals to {2} != {3}", from, to, links[key], weight));
                }
                else
                    links.Add(key, weight);
            }
        }

        private void Verify(int start, int end, ICollection<int> crashNodes)
        {
            if (start == -1)
                throw new InvalidDataException("Start node not found");
            if (end == -1)
                throw new InvalidDataException("End node not found");
            if (crashNodes.Contains(end))
                throw new InvalidDataException("Finish node is in crash status, it can't be reached");
        }

        private IGraph BuildGraph(IEnumerable<int> nodes, IEnumerable<KeyValuePair<Tuple<int, int>, int>> links)
        {
            var graph = new Graph();
            graph.AddNodes(nodes);
            foreach (var link in links)
                graph.AddEdge(link.Key.Item1, link.Key.Item2, link.Value);
            return graph;
        }
    }
}
