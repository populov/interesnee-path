﻿using System;
using System.Collections.Generic;

namespace CarPath.Model
{
    public class Dijkstra : IPathFinder
    {
        public int[] FindPath(IGraph graph, int start, int finish)
        {
            var nodes = new Dictionary<int, PathNode>();
            foreach (var nodeId in graph.GetNodes())
                nodes[nodeId] = new PathNode (nodeId) { Distance = int.MaxValue };

            var startNode = nodes[start];
            startNode.Distance = 0;
            var queue = new Treap<Tuple<int,int>, PathNode>(startNode, PathNode.GetKey, PathNode.GetLessKey, PathNode.KeyComparison);
            while (queue != null)
            {
                var currentNode = queue.Minimum();
                foreach (var edges in graph.GetEdges(currentNode.Id))
                {
                    var node = nodes[edges.Key];
                    if (node.Visited)
                        continue;
                    var newDistance = currentNode.Distance + edges.Value;
                    if (newDistance < node.Distance)
                    {
                        node.Distance = newDistance;
                        node.PreviousNode = currentNode;
                    }
                    queue = queue.Insert(node);
                }
                currentNode.Visited = true;
                queue = queue.Remove(currentNode);
            }

            if (nodes[finish].Distance == int.MaxValue)
                throw new InvalidOperationException("Finish node is unreachable");

            return ReversePath(nodes, finish);
        }

        private int[] ReversePath(Dictionary<int, PathNode> nodes, int finish)
        {
            var path = new List<int>();
            var node = nodes[finish];
            do
            {
                path.Insert(0, node.Id);
                node = node.PreviousNode;
            } while (node != null);
            return path.ToArray();
        }

        public class PathNode
        {
            public PathNode(int id)
            {
                Id = id;
            }

            public int Id { get; private set; }
            public int Distance;
            public PathNode PreviousNode;
            public bool Visited;

            public static Tuple<int, int> GetKey(PathNode node)
            {
                return new Tuple<int, int>(node.Distance, node.Id);
            }

            public static Tuple<int, int> GetLessKey(PathNode node)
            {
                return new Tuple<int, int>(node.Distance, node.Id-1);
            }

            public static int KeyComparison(Tuple<int, int> k1, Tuple<int, int> k2)
            {
                var compareDistance = k1.Item1.CompareTo(k2.Item1);
                if (compareDistance != 0)
                    return compareDistance;
                return k1.Item2.CompareTo(k2.Item2);
            }
        }
    }
}
