﻿using System;
using System.IO;

namespace CarPath.Model
{
    public class PathPrinterSimple : IPathPrinter
    {
        private readonly Stream outStream;

        public PathPrinterSimple(Stream outStream)
        {
            this.outStream = outStream;
        }

        protected virtual void Print(IGraph graph, int[] nodes, TextWriter w)
        {
            w.WriteLine(string.Join(" ", nodes));
        }

        public void Print(IGraph graph, int[] nodes)
        {
            using (var w = new StreamWriter(outStream))
                Print(graph, nodes, w);
        }
    }

    public class PathPrinterWithWeigts : PathPrinterSimple
    {
        public PathPrinterWithWeigts(Stream outStream) : base(outStream) {}

        protected override void Print(IGraph graph, int[] nodes, TextWriter w)
        {
            int pathLength = 0;
            for (int i = 0; i < nodes.Length - 1; i++)
            {
                var from = nodes[i];
                var to = nodes[i + 1];
                var weight = graph.GetEdges(@from)[to];
                pathLength += weight;
                w.Write("[{0}] -{1}-> ", from, weight);
            }
            w.WriteLine("[{0}] {2}Total length: {1}", nodes[nodes.Length - 1], pathLength, Environment.NewLine);
        }
    }
}
