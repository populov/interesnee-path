﻿namespace CarPath.Model
{
    public interface IPathFinder
    {
        int[] FindPath(IGraph graph, int start, int finish);
    }
}
