﻿using System.Collections.Generic;

namespace CarPath.Model
{
    public interface IGraph
    {
        IDictionary<int,int> GetEdges(int nodeId);
        IEnumerable<int> GetNodes();
    }
}
