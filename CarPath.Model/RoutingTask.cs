﻿namespace CarPath.Model
{
    public class RoutingTask
    {
        public RoutingTask(IGraph roadNet, int start, int finish)
        {
            RoadNet = roadNet;
            Start = start;
            Finish = finish;
        }

        public IGraph RoadNet { get; private set; }
        public int Start { get; private set; }
        public int Finish { get; private set; }
    }
}
