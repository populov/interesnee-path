﻿namespace CarPath.Model
{
    public interface IRouteDataReader
    {
        RoutingTask Read(string fileName);
    }
}
