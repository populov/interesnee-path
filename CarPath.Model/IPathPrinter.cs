﻿namespace CarPath.Model
{
    public interface IPathPrinter
    {
        void Print(IGraph graph, int[] nodes);
    }
}
