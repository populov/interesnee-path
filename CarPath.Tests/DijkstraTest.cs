﻿using System;
using CarPath.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class DijkstraTest
    {
        [TestMethod]
        public void PathNodeComplexKeys()
        {
            var one1 = new Dijkstra.PathNode(1) {Distance = 1};
            var one2 = new Dijkstra.PathNode(1) {Distance = 2};
            var two1 = new Dijkstra.PathNode(2) {Distance = 1};
            var two2 = new Dijkstra.PathNode(2) {Distance = 2};

            Func<Dijkstra.PathNode, Dijkstra.PathNode, int> compareKeys = (node1, node2)
                => Dijkstra.PathNode.KeyComparison(Dijkstra.PathNode.GetKey(node1), Dijkstra.PathNode.GetKey(node2));

            Assert.AreEqual(0, compareKeys(one1, one1));
            Assert.AreEqual(-1, compareKeys(one1, one2));
            Assert.AreEqual(1, compareKeys(one2, two1));
            Assert.AreEqual(-1, compareKeys(one2, two2));
            Assert.AreEqual(-1, compareKeys(one1, two1));
            Assert.AreEqual(-1, compareKeys(two1, two2));

            foreach (var node in new[] {one1, one2, two1, two2})
            {
                var key = Dijkstra.PathNode.GetKey(node);
                var lessKey = Dijkstra.PathNode.GetLessKey(node);
                Assert.AreEqual(1, Dijkstra.PathNode.KeyComparison(key, lessKey));
            }
        }

        [TestMethod]
        public void SamplePath()
        {
            var graph = new Graph();
            graph.AddNodes(new[] {1, 2, 3, 4, 5, 6});
            graph.AddEdge(1, 2, 7);
            graph.AddEdge(1, 3, 9);
            graph.AddEdge(1, 6, 14);
            graph.AddEdge(2, 3, 10);
            graph.AddEdge(2, 4, 15);
            graph.AddEdge(3, 4, 11);
            graph.AddEdge(3, 6, 2);
            graph.AddEdge(4, 5, 6);
            graph.AddEdge(5, 6, 9);

            var path = new Dijkstra().FindPath(graph, 1, 5);

            var expected = new[] {1, 3, 6, 5};
            Assert.AreEqual(expected.Length, path.Length);
            for(var i = 0; i < path.Length; i++)
                Assert.AreEqual(expected[i], path[i]);
        }

        [TestMethod]
        public void NoPath()
        {
            var graph = new Graph();
            foreach (var node in new[] { 1, 2, 3, 4 })
                graph.AddNode(node);
            graph.AddEdge(1, 2, 3);
            graph.AddEdge(3, 4, 5);

            ExceptionAssert.Throws<InvalidOperationException>("Finish node is unreachable", () => new Dijkstra().FindPath(graph, 1, 4));
        }
    }
}
