﻿using System;
using System.Linq;
using CarPath.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class GraphTest
    {
        [TestMethod]
        public void AddNode()
        {
            var graph = new Graph();
            Assert.AreEqual(0, graph.GetNodes().Count());

            graph.AddNode(5);
            Assert.AreEqual(1, graph.GetNodes().Count());

            graph.AddNodes(new[] {2, 7});
            Assert.AreEqual(3, graph.GetNodes().Count());
            var nodes = graph.GetNodes().ToArray();
            Assert.IsTrue(nodes.Contains(2));
            Assert.IsTrue(nodes.Contains(5));
            Assert.IsTrue(nodes.Contains(7));

            ExceptionAssert.Throws<ArgumentException>("Node 2 already present", () => graph.AddNode(2));
        }

        [TestMethod]
        public void AddEdge_Bidirectional_Idempotent()
        {
            var graph = new Graph();
            graph.AddNodes(new[] {1, 2, 3});
            Assert.IsNotNull(graph.GetEdges(1));
            Assert.AreEqual(0, graph.GetEdges(1).Count);

            const int from = 1, to = 3, weight = 7;
            graph.AddEdge(from, to, weight);
            
            Assert.AreEqual(1, graph.GetEdges(from).Count);
            Assert.AreEqual(to, graph.GetEdges(from).Keys.First());
            Assert.AreEqual(weight, graph.GetEdges(from)[to]);

            Assert.AreEqual(1, graph.GetEdges(to).Count);
            Assert.AreEqual(from, graph.GetEdges(to).Keys.First());
            Assert.AreEqual(weight, graph.GetEdges(to)[from]);

            // Add same edge way back
            graph.AddEdge(to, from, weight);

            Assert.AreEqual(1, graph.GetEdges(from).Count);
            Assert.AreEqual(to, graph.GetEdges(from).Keys.First());
            Assert.AreEqual(weight, graph.GetEdges(from)[to]);

            Assert.AreEqual(1, graph.GetEdges(to).Count);
            Assert.AreEqual(from, graph.GetEdges(to).Keys.First());
            Assert.AreEqual(weight, graph.GetEdges(to)[from]);
        }

        [TestMethod]
        public void AddEdge_Conflict_Warning()
        {
            var graph = new Graph();
            graph.AddNodes(new[] { 1, 2, 3 });

            const int from = 1, to = 3, weight = 7;
            graph.AddEdge(from, to, weight);

            Assert.AreEqual(1, graph.GetEdges(from).Count);
            Assert.AreEqual(to, graph.GetEdges(from).Keys.First());
            Assert.AreEqual(weight, graph.GetEdges(from)[to]);

            Assert.AreEqual(1, graph.GetEdges(to).Count);
            Assert.AreEqual(from, graph.GetEdges(to).Keys.First());
            Assert.AreEqual(weight, graph.GetEdges(to)[from]);

            // Add same edge way back
            ExceptionAssert.Throws<ArgumentException>("Weight from 3 to 1 is already defined and equals to 7 != 8", () => graph.AddEdge(to, from, weight + 1));
        }

        [TestMethod]
        public void AddEdge_NotExistingNode_Warning()
        {
            var graph = new Graph();
            graph.AddNodes(new[] { 1, 2, 3 });
            ExceptionAssert.Throws<ArgumentException>("Edge to not existing node: 4", () => graph.AddEdge(1, 4, 7));
            ExceptionAssert.Throws<ArgumentException>("Edge from not existing node: 5", () => graph.AddEdge(5, 3, 7));
        }
    }
}
