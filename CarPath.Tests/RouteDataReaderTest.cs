﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using CarPath.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class RouteDataReaderTest
    {
        [TestMethod]
        [DeploymentItem("shortestway-sample.xml")]
        public void Read_ReadSample()
        {
            Assert.IsTrue(File.Exists("shortestway-sample.xml"));
            var data = new RouteDataReader().Read("shortestway-sample.xml");
            Assert.AreEqual(1, data.Start);
            Assert.AreEqual(10, data.Finish);
            Assert.AreEqual(11, data.RoadNet.GetNodes().Count());
            var startEdges = data.RoadNet.GetEdges(data.Start);
            Assert.AreEqual(2, startEdges.Count);
            Assert.AreEqual(5, startEdges[2]);
            Assert.AreEqual(5, startEdges[6]);
        }

        [TestMethod]
        public void Read_IgnoreCrashNodesAndEdges()
        {
            const string roadMap =
@"<graph>
	<node id='1' role='start'>
		<link ref='3' weight='5' />
		<link ref='2' weight='5' />
	</node>
	<node id='2' status='crash'>
		<link ref='1' weight='5' />
		<link ref='3' weight='5' />
	</node>
	<node id='3' role='finish'>
		<link ref='2' weight='5' />
		<link ref='1' weight='5' />
	</node>
</graph>";
            var xDoc = new XmlDocument();
            xDoc.LoadXml(roadMap);
            var data = new RouteDataReader().Parse(xDoc);
            Assert.AreEqual(2, data.RoadNet.GetNodes().Count());
            Assert.AreEqual(1, data.RoadNet.GetEdges(data.Start).Count);
            Assert.AreEqual(5, data.RoadNet.GetEdges(data.Start)[3]);
            Assert.AreEqual(1, data.RoadNet.GetEdges(data.Finish).Count);
            Assert.AreEqual(5, data.RoadNet.GetEdges(data.Finish)[1]);
        }

        [TestMethod]
        public void Read_NoStart()
        {
            const string invalidRoadMap = 
@"<graph>
	<node id='1'>
		<link ref='3' weight='5' />
		<link ref='2' weight='5' />
	</node>
	<node id='2'>
		<link ref='1' weight='5' />
		<link ref='3' weight='5' />
	</node>
	<node id='3' role='finish'>
		<link ref='2' weight='5' />
		<link ref='1' weight='5' />
	</node>
</graph>";
            var xDoc = new XmlDocument();
            xDoc.LoadXml(invalidRoadMap);
            ExceptionAssert.Throws<InvalidDataException>("Start node not found", () => new RouteDataReader().Parse(xDoc));
        }

        [TestMethod]
        public void Read_DoubleStart()
        {
            const string invalidRoadMap =
@"<graph>
	<node id='1' role='start'>
		<link ref='3' weight='5' />
		<link ref='2' weight='5' />
	</node>
	<node id='2' role='start'>
		<link ref='1' weight='5' />
		<link ref='3' weight='5' />
	</node>
	<node id='3' role='finish'>
		<link ref='2' weight='5' />
		<link ref='1' weight='5' />
	</node>
</graph>";
            var xDoc = new XmlDocument();
            xDoc.LoadXml(invalidRoadMap);
            ExceptionAssert.Throws<InvalidDataException>("More than one start node", () => new RouteDataReader().Parse(xDoc));
        }

        [TestMethod]
        public void Build_LinkToAbsentNode()
        {
            const string invalidRoadMap =
@"<graph>
	<node id='1' role='start'>
		<link ref='3' weight='5' />
		<link ref='152' weight='5' />
	</node>
	<node id='2'>
		<link ref='1' weight='5' />
		<link ref='3' weight='5' />
	</node>
	<node id='3' role='finish'>
		<link ref='2' weight='5' />
		<link ref='1' weight='5' />
	</node>
</graph>";
            var xDoc = new XmlDocument();
            xDoc.LoadXml(invalidRoadMap);
            ExceptionAssert.Throws<ArgumentException>("Edge to not existing node: 152", () => new RouteDataReader().Parse(xDoc));
        }

        [TestMethod]
        public void Build_DifferentWeightsForSameEdge()
        {
            const string invalidRoadMap =
@"<graph>
	<node id='1' role='start'>
		<link ref='3' weight='5' />
		<link ref='2' weight='5' />
	</node>
	<node id='2'>
		<link ref='1' weight='15' />
		<link ref='3' weight='5' />
	</node>
	<node id='3' role='finish'>
		<link ref='2' weight='5' />
		<link ref='1' weight='5' />
	</node>
</graph>";
            var xDoc = new XmlDocument();
            xDoc.LoadXml(invalidRoadMap);
            ExceptionAssert.Throws<ArgumentException>("Weight from 2 to 1 is already defined and equals to 5 != 15", () => new RouteDataReader().Parse(xDoc));
        }

        [TestMethod]
        public void Read_RepeatedNodeId()
        {
            const string invalidRoadMap =
@"<graph>
	<node id='1' role='start'>
		<link ref='3' weight='5' />
		<link ref='2' weight='5' />
	</node>
	<node id='2'>
		<link ref='1' weight='5' />
	</node>
	<node id='2' role='finish'>
		<link ref='2' weight='5' />
	</node>
</graph>";
            var xDoc = new XmlDocument();
            xDoc.LoadXml(invalidRoadMap);
            ExceptionAssert.Throws<ArgumentException>("Node 2 already present", () => new RouteDataReader().Parse(xDoc));
        }

        [TestMethod]
        public void Verify_FinishCrashStatus()
        {
            const string invalidRoadMap =
@"<graph>
	<node id='1' role='start'>
		<link ref='3' weight='5' />
		<link ref='2' weight='5' />
	</node>
	<node id='2'>
		<link ref='1' weight='5' />
	</node>
	<node id='3' role='finish' status='crash'>
		<link ref='2' weight='5' />
	</node>
</graph>";
            var xDoc = new XmlDocument();
            xDoc.LoadXml(invalidRoadMap);
            ExceptionAssert.Throws<InvalidDataException>("Finish node is in crash status, it can't be reached", () => new RouteDataReader().Parse(xDoc));
        }
    }
}