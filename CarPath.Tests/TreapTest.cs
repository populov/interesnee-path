﻿using System;
using CarPath.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class TreapTest
    {
        [TestMethod]
        public void HandleSimpleKeys()
        {
            var one = new Item(1, "one");
            var two = new Item(2, "two");
            var three = new Item(3, "three");
            var four = new Item(4, "four");
            var five = new Item(5, "five");

            var queue = new Treap<int, Item>(three, i => i.key, i => i.key-1, (k1,k2) => k1.CompareTo(k2));
            Item.AssertEqual(three, queue.Minimum());

            queue = queue.Insert(five);
            Item.AssertEqual(three, queue.Minimum());

            queue = queue.Insert(two);
            Item.AssertEqual(two, queue.Minimum());

            queue = queue.Insert(one);
            Item.AssertEqual(one, queue.Minimum());

            queue = queue.Insert(four);
            Item.AssertEqual(one, queue.Minimum());

            queue = queue.Remove(queue.Minimum());
            Item.AssertEqual(two, queue.Minimum());

            queue = queue.Remove(queue.Minimum());
            Item.AssertEqual(three, queue.Minimum());

            queue = queue.Remove(queue.Minimum());
            Item.AssertEqual(four, queue.Minimum());

            queue = queue.Remove(queue.Minimum());
            Item.AssertEqual(five, queue.Minimum());

            queue = queue.Remove(queue.Minimum());
            Assert.IsNull(queue);
        }

        [TestMethod]
        public void HandleComplexKeys()
        {
            var one1 = new Item(1, "one1");
            var one2 = new Item(1, "one2");
            var two = new Item(2, "two");
            var three = new Item(3, "three");
            var four1 = new Item(4, "four1");
            var four2 = new Item(4, "four2");
            var five = new Item(5, "five");

            Assert.AreEqual(-1, Item.ComplexKeyComparison(Item.GetComplexKey(one1), Item.GetComplexKey(one2)));
            Assert.AreEqual(1, Item.ComplexKeyComparison(Item.GetComplexKey(one2), Item.GetComplexKey(one1)));
            Assert.AreEqual(0, Item.ComplexKeyComparison(Item.GetComplexKey(one1), Item.GetComplexKey(one1)));

            var queue = new Treap<Tuple<int,string>, Item>(three, Item.GetComplexKey, Item.GetComplexKeyLess, Item.ComplexKeyComparison);
            Item.AssertEqual(three, queue.Minimum());

            queue = queue.Insert(five);
            Item.AssertEqual(three, queue.Minimum());

            queue = queue.Insert(two);
            Item.AssertEqual(two, queue.Minimum());

            queue = queue.Insert(four1);
            Item.AssertEqual(two, queue.Minimum());

            queue = queue.Insert(one1);
            Item.AssertEqual(one1, queue.Minimum());

            queue = queue.Insert(one2);
            Item.AssertEqual(one1, queue.Minimum());

            queue = queue.Insert(four2);
            Item.AssertEqual(one1, queue.Minimum());

            queue = queue.Remove(queue.Minimum());
            Item.AssertEqual(one2, queue.Minimum());

            queue = queue.Remove(queue.Minimum());
            Item.AssertEqual(two, queue.Minimum());

            queue = queue.Remove(queue.Minimum());
            Item.AssertEqual(three, queue.Minimum());

            queue = queue.Remove(queue.Minimum());
            Item.AssertEqual(four1, queue.Minimum());

            queue = queue.Remove(queue.Minimum());
            Item.AssertEqual(four2, queue.Minimum());

            queue = queue.Remove(queue.Minimum());
            Item.AssertEqual(five, queue.Minimum());

            queue = queue.Remove(queue.Minimum());
            Assert.IsNull(queue);
        }

        private struct Item
        {
            public Item(int key, string data)
            {
                this.key = key;
                this.data = data;
            }

            public readonly int key;
            private readonly string data;

            public static Tuple<int,string> GetComplexKey(Item item)
            {
                return new Tuple<int,string>(item.key, item.data);
            }

            public static Tuple<int, string> GetComplexKeyLess(Item item)
            {
                return new Tuple<int, string>(item.key, null);
            }

            public static int ComplexKeyComparison(Tuple<int, string> k1, Tuple<int, string> k2)
            {
                var cmp1 = k1.Item1.CompareTo(k2.Item1);
                if (cmp1 != 0)
                    return cmp1;
                return k1.Item2 == null ? (k2.Item2 == null ? 0 : -1) : k1.Item2.CompareTo(k2.Item2);
            }

            public override string ToString()
            {
                return string.Format("{0}: {1}", key, data);
            }

            public static void AssertEqual(Item i1, Item i2)
            {
                Assert.AreEqual(i1.key, i2.key, string.Format("Expected item: [{0}], actual [{1}]", i1, i2));
                Assert.AreEqual(i1.data, i2.data, string.Format("Expected item: [{0}], actual [{1}]", i1, i2));
            }
        }
    }
}
