﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    public static class ExceptionAssert
    {
        public static void Throws<TException>(string message, Action action)
            where TException : Exception
        {
            try
            {
                action();

                Assert.Fail("Exception of type {0} expected; got none exception", typeof(TException).Name);
            }
            catch (TException ex)
            {
                Assert.AreEqual(message, ex.Message);
            }
            catch (Exception ex)
            {
                Assert.Fail("Exception of type {0} expected; got exception of type {1}", typeof(TException).Name, ex.GetType().Name);
            }
        }
    }
}
