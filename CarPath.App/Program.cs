﻿using System;
using CarPath.Model;

namespace CarPath.App
{
    class Program
    {
        static void Main(string[] args)
        {
            if (Help.IsRequested(args))
                Help.Print();
            else
            {
                var fileName = args.Length > 0 ? args[0] : "input.xml";
                ReadAndProcessFile(new RouteDataReader(), new Dijkstra(), new PathPrinterSimple(Console.OpenStandardOutput()), fileName);
            }
            Console.ReadLine();
        }

        private static void ReadAndProcessFile(IRouteDataReader reader, IPathFinder finder, IPathPrinter pathPrinter, string fileName)
        {
            RoutingTask routeData;
            try
            {
                routeData = reader.Read(fileName);
            }
            catch (Exception e)
            {
                Console.Write("Data read error: " + e.Message);
                return;
            }
            int[] path;
            try
            {
                path = finder.FindPath(routeData.RoadNet, routeData.Start, routeData.Finish);
            }
            catch (Exception e)
            {
                Console.Write("Path search error: " + e.Message);
                return;
            }
            Console.Write("Optimal path is: ");
            pathPrinter.Print(routeData.RoadNet, path);
        }
    }
}
