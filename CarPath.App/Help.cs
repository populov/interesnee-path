﻿using System;
using System.IO;
using System.Reflection;
using System.Text.RegularExpressions;

namespace CarPath.App
{
    public static class Help
    {
        static public bool IsRequested(string[] args)
        {
            if (args.Length == 0)
                return false;
            return new Regex("^(/|-|--)help$", RegexOptions.IgnoreCase).IsMatch(args[0]);
        }

        static public void Print()
        {
            var assembly = Assembly.GetExecutingAssembly();

            using (var stream = assembly.GetManifestResourceStream("CarPath.App.help.txt"))
            {
                if (stream == null)
                    return;
                using (var reader = new StreamReader(stream))
                {
                    var helpMessage = reader.ReadToEnd();
                    Console.WriteLine(helpMessage);
                }
            }
        }
    }
}
